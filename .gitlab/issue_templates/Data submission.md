#### Data description


#### Submission checklist
<!-- Replace [ ] with [x] for checks you have performed. -->

- [ ] Data are provided as either:
  - a link
  - attached to this issue
- [ ] Data are structured as either:
  - CSV files – `survey.csv` (required) plus one or more of the following: `glacier.csv`, `point.csv` (preferred), `band.csv`
  - Excel file or linked Google Sheet with multiple sheets – `survey` (required) plus one or more of the following: `glacier`, `point` (preferred), `band`
- [ ] Each table contains only:
  - Column names in the first row (header)
  - Column values in the proceeding rows
- [ ] Column names and values match the "Data structure" described in the data [documentation](https://gitlab.com/wgms/glathida/-/blob/main/documentation.md#data-structure)
