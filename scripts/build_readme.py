import dbc.util

package = dbc.util.read_yaml('datapackage.yaml')
txt = dbc.util.render_template(
  'scripts/readme.md.jinja', {'package': package}
)
with open('documentation.md', 'w') as file:
  file.write(txt)
