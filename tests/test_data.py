import pytest

from . import helpers
from .setup import report, package


if package.metadata_errors:
  pytest.skip("Skipping data tests", allow_module_level=True)


def test_data():
  helpers.print_report(report)
  assert report.valid
