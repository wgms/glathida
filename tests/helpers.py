from collections import defaultdict
from pathlib import Path
from typing import Any, Dict, Iterable, Union
import warnings

import pandas as pd
from validator.schema import Report

from .schemas import BEFORE_FRICTIONLESS, FRICTIONLESS, AFTER_FRICTIONLESS


def read_data() -> Dict[str, pd.DataFrame]:
  paths = Path('data').rglob('*.csv')
  dfs = defaultdict(list)
  for path in paths:
    dfs[path.stem].append(pd.read_csv(path, low_memory=False))
  return {
    key: pd.concat(value, ignore_index=True).convert_dtypes()
    for key, value in dfs.items()
  }


def read_csvs(
  paths: Iterable[Union[str, Path]],
  **kwargs: Any
) -> Dict[str, pd.DataFrame]:
  return {path.stem: pd.read_csv(path, **kwargs) for path in paths}


def write_csvs(
  dfs: Dict[str, pd.DataFrame],
  path: Union[str, Path],
  index: bool = False,
  **kwargs: Any
) -> None:
  Path(path).mkdir(exist_ok=True, parents=True)
  for key, df in dfs.items():
    df.to_csv(Path(path) / f'{key}.csv', index=index, **kwargs)


def read_excel(
  path: Union[str, Path],
  **kwargs: Any
) -> Dict[str, pd.DataFrame]:
  book = pd.ExcelFile(path)
  dfs = {}
  for name in book.sheet_names:
    with warnings.catch_warnings():
      # Suppress openpyxl warnings that validation and formatting are ignored
      warnings.simplefilter('ignore')
      df = book.parse(sheet_name=name, **kwargs)
    dfs[name] = df
  return dfs


def print_report(report: Report) -> None:
  if report.valid:
    print('Valid data [true]')
    print(report)
  else:
    print('Valid data [false]')
    print(report)
    df = report.to_dataframe()
    # errors
    is_error = df['code'] == 'error'
    if is_error.any():
      print('[ERRORS]')
      print(df[is_error][['table', 'column', 'row', 'check']])
    # failures
    is_fail = df['code'] == 'fail'
    if is_fail.any():
      print('[FAILURES]')
      print(df[is_fail][['table', 'column', 'row', 'check']])


def read_submission(
  path: Union[str, Path],
  to_csvs: bool = False,
  dtype: Any = None
) -> Dict[str, pd.DataFrame]:
  path = Path(path)
  files = [x for x in path.iterdir() if x.is_file()]
  excel_files = [x for x in files if x.suffix.lower() in ('.xls', '.xlsx')]
  csv_files = [x for x in files if x.suffix.lower() == '.csv']
  if csv_files:
    return read_csvs(csv_files, dtype=dtype)
  if excel_files:
    if len(excel_files) > 1:
      raise ValueError(f'Multiple Excel files found: {excel_files}')
    dfs = read_excel(excel_files[0], dtype=dtype)
    if to_csvs:
      write_csvs(dfs, path)
      dfs = read_submission(path)
    return dfs
  raise ValueError(f'No suitable files found among {files}')


def validate(dfs: Dict[str, pd.DataFrame]) -> Report:
  validation = BEFORE_FRICTIONLESS + FRICTIONLESS + AFTER_FRICTIONLESS
  return validation(dfs)


def increment_submission_ids(
  path: Union[str, Path],
  output: Union[str, Path] = None
) -> None:
  # Look up the next survey and glacier ids
  db = read_data()
  next_survey_id = db['survey']['id'].astype(int).max() + 1
  next_glacier_id = db['glacier']['id'].astype(int).max() + 1
  # Read the submission
  dfs = read_submission(path, dtype='string')
  # Increment survey and glacier ids
  survey_ids = dfs['survey']['id'].astype(int).drop_duplicates().sort_values()
  survey_map = dict(
    zip(
      survey_ids.astype(str),
      range(next_survey_id, next_survey_id + len(survey_ids))
    )
  )
  dfs['survey']['id'] = dfs['survey']['id'].map(survey_map)
  if 'point' in dfs:
    dfs['point']['survey_id'] = dfs['point']['survey_id'].map(survey_map)
  if 'glacier' in dfs:
    glacier_ids = dfs['glacier']['id'].astype(int).drop_duplicates().sort_values()
    glacier_map = dict(
      zip(
        glacier_ids.astype(str),
        range(next_glacier_id, next_glacier_id + len(glacier_ids))
      )
    )
    dfs['glacier']['id'] = dfs['glacier']['id'].map(glacier_map)
    dfs['glacier']['survey_id'] = dfs['glacier']['survey_id'].map(survey_map)
    if 'band' in dfs:
      dfs['band']['glacier_id'] = dfs['band']['glacier_id'].map(glacier_map)
    if 'point' in dfs:
      dfs['point']['glacier_id'] = dfs['point']['glacier_id'].map(glacier_map)
  # Check the submission
  report = validate(dfs)
  assert report.valid, print_report(report)
  # Overwrite submission
  if output is None:
    output = path
  else:
    Path(output).mkdir(exist_ok=True, parents=True)
  write_csvs(dfs, output or path)
