#!/usr/bin/env python
from pathlib import Path
import sys

from . import helpers


if __name__ == '__main__':
  path = Path(sys.argv[1])
  helpers.increment_submission_ids(path, output=Path('data') / path.name)
