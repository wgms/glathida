#!/usr/bin/env python
from pathlib import Path
import sys

from . import helpers


if __name__ == '__main__':
  path = Path(sys.argv[1])
  dfs = helpers.read_submission(path, dtype='string')
  report = helpers.validate(dfs)
  helpers.print_report(report)
  assert report.valid
