import frictionless

from . import helpers


package = frictionless.Package('datapackage.yaml')
if not package.metadata_errors:
  dfs = helpers.read_data()
  report = helpers.validate(dfs)
